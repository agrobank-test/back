FROM node:16-alpine as build

WORKDIR /app
RUN chmod -R 777 /app
COPY package.json ./
COPY yarn.lock ./

RUN yarn --silent

COPY . .

RUN yarn prisma:generate
# Build the NestJS app
RUN yarn build

# Expose the port on which the app will run (change it according to your app's configuration)
EXPOSE 3000

# Start the NestJS app
CMD [ "yarn", "start:prod" ]
