import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PasswordService } from 'src/user/password.service';
import { UserService } from 'src/user/user.service';
import { ChatController } from './chat.controller';
import { ChatGateway } from './chat.gateway';
import { ChatService } from './chat.service';

@Module({
  providers: [
    ChatGateway,
    ChatService,
    UserService,
    PasswordService,
    JwtService,
  ],
  controllers: [ChatController],
})
export class ChatModule {}
