import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserGuard } from 'src/user/user.guard';
import { ChatService } from './chat.service';
import { SendMessageDto } from './dto/send-message.dto';
import { User, UserRole } from '@prisma/client';
import { UserRolesGuard } from 'src/user/user-roles.guard';
import { UserRoles } from 'src/user/user-roles.decorator';

@ApiTags('Chat')
@Controller('chat')
@UseGuards(UserGuard)
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @UserRoles(UserRole.ADMIN)
  @UseGuards(UserRolesGuard)
  @ApiOperation({
    summary: 'Returns all chats',
    description: 'Returns all chats. `[ADMIN ONLY]`',
  })
  @Get('all')
  findAll() {
    return this.chatService.findAll();
  }

  @UserRoles(UserRole.CUSTOMER)
  @ApiOperation({
    summary: "Returns CUSTOMER's chat",
    description: "Returns CUSTOMER's chat according to user id",
  })
  @Get('my')
  findMyChat(@Req() req) {
    return this.chatService.findChatByUserId((req.user as User).id);
  }

  @UserRoles(UserRole.ADMIN, UserRole.CUSTOMER)
  @ApiOperation({
    summary: 'Sends message',
    description:
      'Sends message according to `receiverId` or `chatId` if user is an ADMIN, otherwise sends message to all admins (no need to specify user or chat). Returns true/false based on success',
  })
  @Post('send-message')
  sendMessage(@Body() sendMessageDto: SendMessageDto, @Req() req) {
    return this.chatService.sendMessage(sendMessageDto, req.user as User);
  }

  @UserRoles(UserRole.ADMIN, UserRole.CUSTOMER)
  @ApiOperation({
    summary: 'Returns chat with all messages',
    description: 'Returns chat with all messages for chat using `chatId`',
  })
  @Get(':id')
  findMessages(@Param('id') chatId: string) {
    return this.chatService.findById(chatId);
  }
}
