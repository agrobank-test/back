import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Chat, Message } from '@prisma/client';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  cors: true,
})
export class ChatGateway {
  @WebSocketServer()
  server: Server;

  sendMessage(receiverId: string | string[], message: Message) {
    this.server.to(receiverId).emit('message', message);
  }

  newChat(receiverId: string | string[], chat: Chat) {
    this.server.to(receiverId).emit('new-chat', chat);
  }

  @SubscribeMessage('join-room')
  createRoom(@MessageBody() data: string, @ConnectedSocket() client: Socket) {
    return client.join(data);
  }
}
