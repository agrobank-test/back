import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { SendMessageDto } from './dto/send-message.dto';
import { PrismaService } from 'nestjs-prisma';
import { ChatGateway } from './chat.gateway';
import { User } from '@prisma/client';

@Injectable()
export class ChatService {
  constructor(
    private prisma: PrismaService,
    private readonly chatGateway: ChatGateway,
  ) {}

  private async getChat(userId: string, chatId?: string) {
    if (chatId) return this.prisma.chat.findUnique({ where: { id: chatId } });

    const chat = await this.prisma.chat.findFirst({
      where: { userId },
    });

    if (!chat) return this.prisma.chat.create({ data: { userId } });

    return chat;
  }

  async sendMessage(
    { text, chatId, receiverId }: SendMessageDto,
    sender: User,
  ) {
    try {
      const isAdmin = sender.role === 'ADMIN';

      if (isAdmin && !(receiverId || chatId))
        throw new BadRequestException(
          'This method requires either `receiverId` or `chatId` for ADMIN users.',
        );

      const chat = await this.getChat(isAdmin ? receiverId : sender.id, chatId);

      const message = await this.prisma.message.create({
        data: { text, chatId: chat.id, senderId: sender.id },
        include: {
          sender: {
            select: {
              name: true,
            },
          },
        },
      });

      const adminIds = isAdmin
        ? []
        : (
            await this.prisma.user.findMany({
              where: { role: 'ADMIN' },
              select: { id: true },
            })
          ).map(({ id }) => id);

      if (isAdmin) this.chatGateway.sendMessage(chat.userId, message);
      else {
        this.chatGateway.newChat(adminIds, chat);
        this.chatGateway.sendMessage(adminIds, message);
      }

      return message;
    } catch (error) {
      Logger.error(error);
      throw new InternalServerErrorException('Something went wrong.');
    }
  }

  async findChatByUserId(userId: string) {
    const chat = await this.prisma.chat.findUnique({
      where: { userId },
      include: {
        messages: {
          orderBy: { createdAt: 'asc' },
          include: { sender: { select: { name: true } } },
        },
      },
    });

    if (!chat) {
      return this.prisma.chat.create({
        data: {
          userId,
        },
      });
    }

    return chat;
  }

  findAll() {
    return this.prisma.chat.findMany({
      include: {
        user: { select: { name: true, avatar: true } },
        messages: {
          orderBy: { createdAt: 'desc' },
          take: 1,
        },
      },
    });
  }

  findById(chatId: string) {
    return this.prisma.chat.findUnique({
      where: { id: chatId },
      include: {
        messages: {
          orderBy: { createdAt: 'asc' },
          include: { sender: { select: { name: true } } },
        },
      },
    });
  }
}
