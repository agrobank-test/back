import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class SendMessageDto {
  @ApiProperty()
  @IsString()
  text: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  receiverId?: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  chatId?: string;
}
