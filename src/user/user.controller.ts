import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserGuard } from './user.guard';
import { LoginDto } from './dto/login.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({
    summary: 'Returns JWT token',
    description: 'Returns JWT token using `username` and `password`',
  })
  @Post('sign-in')
  signIn(@Body() loginDto: LoginDto) {
    return this.userService.signIn(loginDto);
  }

  @ApiOperation({
    summary: 'Returns current user',
    description: 'Returns current user using JWT token',
  })
  @Get('me')
  @UseGuards(UserGuard)
  getMe(@Req() req) {
    return req.user;
  }

  @ApiOperation({
    summary: 'Creates a new user',
    description: 'Creates a new user',
  })
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @ApiOperation({
    summary: 'Returns all users',
    description: 'Returns all users',
  })
  @UseGuards(UserGuard)
  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @ApiOperation({
    summary: 'Returns a user',
    description: 'Returns a user by `userId`',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @ApiOperation({
    summary: 'Updates a user',
    description: 'Updates a user by `userId`',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @ApiOperation({
    summary: 'Deletes a user ',
    description: 'Deletes a user by `userId`',
  })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }
}
