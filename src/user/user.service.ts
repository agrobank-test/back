import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'nestjs-prisma';
import { LoginDto } from './dto/login.dto';
import { PasswordService } from './password.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { excludePassword } from 'src/utils/excludeFields';

@Injectable()
export class UserService {
  constructor(
    private prisma: PrismaService,
    private readonly passwordService: PasswordService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async signIn({ password, username }: LoginDto) {
    const jwtSecret = this.configService.get<string>('JWT_ACCESS_SECRET');
    const user = await this.prisma.user.findUnique({ where: { username } });
    const isValidPassword = this.passwordService.validatePassword(
      password,
      user.password,
    );

    if (!isValidPassword)
      throw new UnauthorizedException('Invalid credentials.');

    const payload = { sub: user.id, username };

    return {
      token: await this.jwtService.signAsync(payload, { secret: jwtSecret }),
    };
  }

  async create(createUserDto: CreateUserDto) {
    const password =
      createUserDto.password &&
      (await this.passwordService.hashPassword(createUserDto.password));

    return this.prisma.user.create({
      data: { ...createUserDto, password },
      select: excludePassword,
    });
  }

  findAll() {
    return this.prisma.user.findMany({
      select: excludePassword,
    });
  }

  findOne(id: string) {
    return this.prisma.user.findUnique({
      where: { id },
      select: excludePassword,
    });
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const password =
      updateUserDto.password &&
      (await this.passwordService.hashPassword(updateUserDto.password));
    return this.prisma.user.update({
      where: { id },
      data: { ...updateUserDto, password },
      select: excludePassword,
    });
  }

  async remove(id: string) {
    return this.prisma.$transaction(async (trx) => {
      const chat = await trx.chat.findUnique({ where: { userId: id } });

      if (chat) {
        await trx.message.deleteMany({ where: { chatId: chat.id } });
        await trx.chat.delete({ where: { id: chat.id } });
      }

      return trx.user.delete({ where: { id }, select: excludePassword });
    });
  }
}
