import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserSeederService } from './user.seeder';
import { PasswordService } from './password.service';
import { JwtService } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';

@Module({
  controllers: [UserController],
  providers: [
    UserService,
    UserSeederService,
    PasswordService,
    JwtService,
    JwtStrategy,
  ],
})
export class UserModule {}
