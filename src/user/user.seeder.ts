import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { PasswordService } from 'src/user/password.service';

@Injectable()
export class UserSeederService implements OnModuleInit {
  constructor(
    private prisma: PrismaService,
    private readonly passwordService: PasswordService,
  ) {}

  async onModuleInit() {
    try {
      const users = await this.prisma.user.findMany({
        where: { role: 'ADMIN' },
      });
      if (users.length === 0) {
        console.log('Seeding data...');
        await this.seedUsers();
      }
    } catch (error) {
      console.error('Seeding failed', error);
    }
  }

  private async seedUsers() {
    const user1 = await this.prisma.user.create({
      data: {
        name: 'Jonibek Sattorov',
        username: 'admin',
        role: 'ADMIN',
        password: await this.passwordService.hashPassword('admin'),
      },
    });

    const user2 = await this.prisma.user.create({
      data: {
        name: 'Xolida Kamronova',
        username: 'customer',
        role: 'CUSTOMER',
        password: await this.passwordService.hashPassword('customer'),
      },
    });

    return [user1, user2];
  }
}
